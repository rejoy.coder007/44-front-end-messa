
<div class="might-like-section">
    <div class="container">
        <h2>Similar...</h2>
        <div class="might-like-grid">


            @foreach ($possibleLikes as $possibleLike)
                <a href="{{ route('shop.show.item', $possibleLike->slug) }}" class="might-like-product">
                    <img src="{{ asset('img/aa_header/1.jpg') }}" alt="product">
                    <div class="might-like-product-name">{{ $possibleLike->name }}</div>
                    <div class="might-like-product-price">{{ $possibleLike->formatPrice() }}</div>
                </a>
            @endforeach





        </div>
    </div>
</div>

