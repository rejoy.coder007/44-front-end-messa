



    <div class="breadcrumbs">
        <div class="container">
            <a href="/">Home</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span><a href="{{ route('shop.full') }}">Shop</a></span>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>Laptop</span>
        </div>
    </div> <!-- end breadcrumbs -->

    <div class="product-section container">
        <div class="product-section-image">
            <img src="{{ asset('img/aa_header/1.jpg') }}" alt="product">
        </div>
        <div class="product-section-information">
            <h1 class="product-section-title">{{$selected_product->name}}</h1>
            <div class="product-section-subtitle">{{$selected_product->details}}</div>
            <div class="product-section-price">{{$selected_product->formatPrice()}}</div>

            <p>
                {{$selected_product->description}}            </p>

            <p>&nbsp;</p>


                <form action="{{route('cart.store') }}" method="POST">
                  {{ csrf_field() }}

                    <input type="hidden" name ="id" value="{{$selected_product->id}}">
                    <input type="hidden" name ="name" value="{{$selected_product->name}}">
                    <input type="hidden" name ="price" value="{{$selected_product->price}}">




                    <button type="submit" class="button button-plain">Add to Cart</button>
                </form>



        </div>
    </div> <!-- end product-section -->

    <div class="might-like-section">
        <div class="container">
            <h2>Similar...</h2>
            <div class="might-like-grid">


                @foreach ($mightAlsoLikes as $mightAlsoLike)
                    <a href="{{ route('shop.show.item', $mightAlsoLike->slug) }}" class="might-like-product">
                        <img src="{{ asset('img/aa_header/1.jpg') }}" alt="product">
                        <div class="might-like-product-name">{{ $mightAlsoLike->name }}</div>
                        <div class="might-like-product-price">{{ $mightAlsoLike->formatPrice() }}</div>
                    </a>
                @endforeach


                {{--

                <a href="#" class="might-like-product">
                    <img src="{{ asset('img/aa_header/1.jpg') }}" alt="product">
                    <div class="might-like-product-name">Laptop</div>
                    <div class="might-like-product-price">$2499.99</div>
                </a>
                <a href="#" class="might-like-product">
                    <img src="{{ asset('img/aa_header/1.jpg') }}" alt="product">
                    <div class="might-like-product-name">Laptop</div>
                    <div class="might-like-product-price">$2499.99</div>
                </a>
                <a href="#" class="might-like-product">
                    <img src="{{ asset('img/aa_header/1.jpg') }}" alt="product">
                    <div class="might-like-product-name">Laptop</div>
                    <div class="might-like-product-price">$2499.99</div>
                </a>
                <a href="#" class="might-like-product">
                    <img src="{{ asset('img/aa_header/1.jpg') }}" alt="product">
                    <div class="might-like-product-name">Laptop</div>
                    <div class="might-like-product-price">$2499.99</div>
                </a>

                --}}
            </div>
        </div>
    </div>



     
      