@extends('AD_CART.base')

@section('title')
Welcome to Cart
@endsection

@section('laravelBlock')
    @include('AD_CART.aa_include.zd_laravel_files')
@endsection

@section('cssBlock')

@endsection

@section('content')


    @include('AD_CART.aa_include.zc_navbar')

    @include('zz_body.AD_CART.body')




@endsection

@section('cssBlockExtra')

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


@endsection


@section('bottomJS')



@endsection


 
  