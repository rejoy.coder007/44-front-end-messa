<?php

namespace App\Http\Controllers\aa_ControllerTest;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller as CC;

class aaShowProfile extends CC
{
    public function __invoke($id)
    {
        return view("sample_html_param", ['heading' => $id]);
    }
}
