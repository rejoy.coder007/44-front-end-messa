<?php

namespace App\Http\Controllers\za_Cart;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\aa_ProductWeb;
class CartMangementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

       // $possibleLikes = aa_ProductWeb::inRandomOrder()->take(4)->get();

        $possibleLikes = aa_ProductWeb::GiveRandomLikes()->get();
        //  dd($product);

        // return view("AC_PRODUCT_DETAILS.main")->with('selected_product',$selected_product);


        return view("AD_CART.main")->with([
          //  'selected_product' => $selected_product,
            'possibleLikes' => $possibleLikes,

        ]);

    }


    public function store(Request $product)
    {

        /*
        $duplicates = Cart::search(function ($cartItem, $rowId) use ($product) {
            return $cartItem->id === $product->id;
        });

        if ($duplicates->isNotEmpty()) {
            return redirect()->route('cart.index')->with('success_message', 'Item is already in your cart!');
        }
        */

       // dd($product->id);


        $duplicates = Cart::search(function ($cartItem, $rowId) use ($product) {
            return $cartItem->id === $product->id;
        });

        if ($duplicates->isNotEmpty()) {
            return redirect()->route('cart.index')->with('success_message', 'Item is already in your cart!');
        }


        Cart::add($product->id, $product->name, 1, $product->price)
            ->associate('App\aa_ProductWeb');

      //  dd("success");



        return redirect()->route('cart.index')->with('success_message', 'Item was added to your cart!');
    }


    public function destroy($id)
    {
        Cart::remove($id);

       // dd("yes");
        return back()->with('success_message', 'Item has been removed!');
    }


    public function cart_save_later($id)
    {
        $item = Cart::get($id);

        Cart::remove($id);

        $duplicates = Cart::instance('saveForLater')->search(function ($cartItem, $rowId) use ($id) {
            return $rowId === $id;
        });

        if ($duplicates->isNotEmpty()) {
            return redirect()->route('cart.index')->with('success_message', 'Item is already Saved For Later!');
        }

        Cart::instance('saveForLater')->add($item->id, $item->name, 1, $item->price)
            ->associate('App\aa_ProductWeb');

        return redirect()->route('cart.index')->with('success_message', 'Item has been Saved For Later!');
    }


}
