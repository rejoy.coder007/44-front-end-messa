html
{
      -webkit-box-sizing: border-box;
      box-sizing: border-box;
}

*, *:before, *:after
{
      -webkit-box-sizing: inherit;
      box-sizing: inherit;
}

body
{
      font-family: 'Roboto', Arial, sans-serif;
      font-size: 18px;
      font-weight: 300;
      line-height: 1.6;
}

a
{
      color: #212121;
      text-decoration: none;
}

a:visited
{
      color: #212121;
}

h1, h2
{
      font-family: 'Montserrat', Arial, sans-serif;
      font-weight: bold;
}

h1
{
      font-size: 38px;
      margin-bottom: 40px;
}

h2
{
      font-size: 22px;
      margin-bottom: 10px;
}

img
{
      max-width: 100%;
}

.text-center
{
      text-align: center;
}

.container
{
      margin: auto;
      max-width: 1200px;
}

.button
{
      border: 1px solid #212121;
      padding: 12px 40px;
}

.button:hover
{
      background: #212121;
      color: #e9e9e9;
}

.button-white
{
      border: 1px solid #e9e9e9;
      color: #e9e9e9 !important;
}

.button-white:hover
{
      background: #e9e9e9;
      color: #212121 !important;
}

.section-description
{
      margin: auto;
      width: 80%;
}

.button-container
{
      margin: 80px 0;
}

header
{
      -webkit-background-size: cover;
      background-image: -webkit-gradient(linear, left top, right bottom, from(rgba(255, 0, 0, 0.8)), to(rgba(40, 180, 133, 0.1))), url("/img/aa_header/aa_bg.jpg");
      background-image: -webkit-linear-gradient(left top, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-image: -o-linear-gradient(left top, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-image: linear-gradient(to right bottom, rgba(255, 0, 0, 0.8), rgba(40, 180, 133, 0.1)), url("/img/aa_header/aa_bg.jpg");
      background-size: cover;
      color: white;
}

header .header__logo
{
      -webkit-border-radius: 50%;
      border-radius: 50%;
      height: 40px;
      width: 40px;
}

.hero
{
      -ms-grid-columns: 1fr 1fr;
      display: -ms-grid;
      display: grid;
      grid-gap: 30px;
      grid-template-columns: 1fr 1fr;
      padding-bottom: 84px;
      padding-top: 20px;
}

.hero .hero-image
{
      padding-left: 60px;
}

.hero h1
{
      font-size: 52px;
      margin-top: 50px;
}

.hero p
{
      margin: 40px 0 68px;
}

.hero .button
{
      margin-right: 14px;
}

.featured-section
{
      padding: 50px 0;
}

.featured-section .products
{
      -ms-grid-columns: 1fr 1fr 1fr 1fr;
      display: -ms-grid;
      display: grid;
      grid-gap: 30px;
      grid-template-columns: 1fr 1fr 1fr 1fr;
}

.testimony-section
{
      background: #F5F5F5;
      border-top: 1px solid #CDCDCD;
      padding: 50px 0;
}

.testimony-section .testimony-posts
{
      -ms-grid-columns: 1fr 30px 1fr 30px 1fr;
      display: -ms-grid;
      display: grid;
      grid-gap: 30px;
      grid-template-areas: "testimony2 testimony1 testimony3";
      grid-template-columns: repeat(3, 1fr);
      margin: 60px 0 60px;
}

.testimony-section .testimony-posts #testimony1
{
      -ms-grid-column: 3;
      -ms-grid-row: 1;
      grid-area: testimony1;
}

.testimony-section .testimony-posts #testimony2
{
      -ms-grid-column: 1;
      -ms-grid-row: 1;
      grid-area: testimony2;
}

.testimony-section .testimony-posts #testimony2
{
      -ms-grid-column: 5;
      -ms-grid-row: 1;
      grid-area: testimony3;
}

footer
{
      background: #535353;
      color: #e9e9e9;
      padding: 40px 0;
}

footer .footer-content
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
}

footer .footer-content ul
{
      -ms-flex-pack: justify;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      justify-content: space-between;
      width: 30%;
}

footer .footer-content ul a
{
      color: #e9e9e9;
}

@media only screen and (max-width: 1200px)
{
      .container
      {
            max-width: 960px;
      }
}

@media only screen and (max-width: 992px)
{
      header .top-nav
      {
            -ms-flex-direction: column;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            -webkit-flex-direction: column;
            flex-direction: column;
      }

      header .top-nav .logo
      {
            margin: auto;
      }

      header .top-nav ul
      {
            margin: 20px auto 0;
            width: 95%;
      }

      header .hero
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
            text-align: center;
      }

      header .hero .hero-image
      {
            margin-top: 40px;
            padding-left: 0;
      }

      .featured-section
      {
            padding: 50px 0;
      }

      .featured-section .products
      {
            -ms-grid-columns: 1fr;
            grid-template-columns: 1fr;
      }

      .testimony-section .testimony-posts
      {
            -ms-grid-columns: 1fr;
            -ms-grid-rows: auto 30px auto 30px auto;
            grid-template-areas: "testimony1" "testimony2" "testimony3";
            grid-template-columns: 1fr;
            text-align: center;
      }

      footer .footer-content
      {
            -ms-flex-direction: column;
            -webkit-box-direction: normal;
            -webkit-box-orient: vertical;
            -webkit-flex-direction: column;
            flex-direction: column;
      }

      footer .footer-content .made-with
      {
            margin: auto;
      }

      footer .footer-content ul
      {
            margin: 20px auto;
            width: 90%;
      }

      .testimony-section .testimony-posts #testimony1
      {
            -ms-grid-column: 1;
            -ms-grid-row: 1;
      }

      .testimony-section .testimony-posts #testimony2
      {
            -ms-grid-column: 1;
            -ms-grid-row: 3;
      }

      .testimony-section .testimony-posts #testimony2
      {
            -ms-grid-column: 1;
            -ms-grid-row: 5;
      }
}

.navigation__checkbox
{
      display: none;
}

.navigation__button
{
      -webkit-border-radius: 50%;
      -webkit-box-shadow: 0 1rem 3rem rgba(0, 0, 0, 0.1);
      background-color: #fff;
      border-radius: 50%;
      box-shadow: 0 1rem 3rem rgba(0, 0, 0, 0.1);
      cursor: pointer;
      height: 5rem;
      position: fixed;
      right: 2rem;
      text-align: center;
      top: 3rem;
      width: 5rem;
      z-index: 2000;
}

@media only screen and (max-width: 56.25em)
{
      .navigation__button
      {
            height: 4rem;
            right: 1rem;
            top: 1rem;
            width: 4rem;
      }
}

@media only screen and (max-width: 37.5em)
{
      .navigation__button
      {
            height: 4rem;
            right: 1rem;
            top: 1rem;
            width: 4rem;
      }
}

.navigation__background
{
      -o-transition: transform 0.8s cubic-bezier(0.86, 0, 0.07, 1);
      -webkit-background-size: cover;
      -webkit-border-radius: 50%;
      -webkit-transition: -webkit-transform 0.8s cubic-bezier(0.86, 0, 0.07, 1);
      background-image: -webkit-gradient(linear, left top, right bottom, from(red), to(rgba(40, 180, 133, 0.9)));
      background-image: -webkit-linear-gradient(left top, red, rgba(40, 180, 133, 0.9));
      background-image: -o-linear-gradient(left top, red, rgba(40, 180, 133, 0.9));
      background-image: linear-gradient(to right bottom, red, rgba(40, 180, 133, 0.9));
      background-size: cover;
      border-radius: 50%;
      height: 4rem;
      position: fixed;
      right: 2.5rem;
      top: 3.5rem;
      transition: -webkit-transform 0.8s cubic-bezier(0.86, 0, 0.07, 1);
      transition: transform 0.8s cubic-bezier(0.86, 0, 0.07, 1);
      transition: transform 0.8s cubic-bezier(0.86, 0, 0.07, 1), -webkit-transform 0.8s cubic-bezier(0.86, 0, 0.07, 1);
      width: 4rem;
      z-index: 1000;
}

@media only screen and (max-width: 56.25em)
{
      .navigation__background
      {
            height: 3rem;
            right: 1.5rem;
            top: 1.5rem;
            width: 3rem;
      }
}

@media only screen and (max-width: 37.5em)
{
      .navigation__background
      {
            height: 3rem;
            right: 1.5rem;
            top: 1.5rem;
            width: 3rem;
      }
}

.navigation__nav
{
      -o-transition: all 0.8s cubic-bezier(0.68, -0.55, 0.265, 1.55);
      -webkit-transition: all 0.8s cubic-bezier(0.68, -0.55, 0.265, 1.55);
      height: 100vh;
      left: 0;
      opacity: 0;
      position: fixed;
      top: 0;
      transition: all 0.8s cubic-bezier(0.68, -0.55, 0.265, 1.55);
      width: 0;
      z-index: 1500;
}

.navigation__list
{
      -ms-transform: translate(-50%, -50%);
      -webkit-transform: translate(-50%, -50%);
      left: 50%;
      list-style: none;
      position: absolute;
      text-align: center;
      top: 50%;
      transform: translate(-50%, -50%);
      width: 100%;
}

.navigation__item
{
      margin: 1rem;
}

.navigation__link:link, .navigation__link:visited
{
      -o-transition: all .4s;
      -webkit-background-size: 220% 220%;
      -webkit-transition: all .4s;
      background-image: -webkit-linear-gradient(330deg, transparent 0%, transparent 50%, #fff 50%);
      background-image: -o-linear-gradient(330deg, transparent 0%, transparent 50%, #fff 50%);
      background-image: linear-gradient(120deg, transparent 0%, transparent 50%, #fff 50%);
      background-size: 220%;
      color: #fff;
      display: inline-block;
      font-size: 2rem;
      font-weight: 300;
      padding: 1rem 2rem;
      text-decoration: none;
      text-transform: uppercase;
      transition: all .4s;
}

.navigation__link:link span, .navigation__link:visited span
{
      display: inline-block;
      margin-right: 1.5rem;
}

.navigation__link:hover, .navigation__link:active
{
      -ms-transform: translateX(1rem);
      -webkit-transform: translateX(1rem);
      background-position: 100%;
      color: #ff66cc;
      transform: translateX(1rem);
}

.navigation__checkbox:checked ~ .navigation__background
{
      -ms-transform: scale(80);
      -webkit-transform: scale(80);
      transform: scale(80);
}

.navigation__checkbox:checked ~ .navigation__nav
{
      opacity: 1;
      width: 100%;
}

.navigation__icon
{
      margin-top: 2.5rem;
      position: relative;
}

@media only screen and (max-width: 56.25em)
{
      .navigation__icon
      {
            margin-top: 2.0rem;
      }
}

@media only screen and (max-width: 37.5em)
{
      .navigation__icon
      {
            margin-top: 2.0rem;
      }
}

.navigation__icon, .navigation__icon::before, .navigation__icon::after
{
      background-color: #333;
      display: inline-block;
      height: 2px;
      width: 3rem;
}

.navigation__icon::before, .navigation__icon::after
{
      -o-transition: all .2s;
      -webkit-transition: all .2s;
      content: "";
      left: 0;
      position: absolute;
      transition: all .2s;
}

.navigation__icon::before
{
      top: -.8rem;
}

.navigation__icon::after
{
      top: .8rem;
}

.navigation__button:hover .navigation__icon::before
{
      top: -1rem;
}

.navigation__button:hover .navigation__icon::after
{
      top: 1rem;
}

.navigation__checkbox:checked + .navigation__button .navigation__icon
{
      background-color: transparent;
}

.navigation__checkbox:checked + .navigation__button .navigation__icon::before
{
      -ms-transform: rotate(135deg);
      -webkit-transform: rotate(135deg);
      top: 0;
      transform: rotate(135deg);
}

.navigation__checkbox:checked + .navigation__button .navigation__icon::after
{
      -ms-transform: rotate(-135deg);
      -webkit-transform: rotate(-135deg);
      top: 0;
      transform: rotate(-135deg);
}
