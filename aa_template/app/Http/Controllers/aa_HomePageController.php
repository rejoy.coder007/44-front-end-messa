<?php

namespace App\Http\Controllers;

use App\aa_ProductWeb;
use Illuminate\Http\Request;

class aa_HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $random_products = aa_ProductWeb::inRandomOrder()->take(8)->get();

        return view("AA_HOME.main")->with('random_products',$random_products);
      //  dd($random_products);
    }


}
