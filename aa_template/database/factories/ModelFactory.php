<?php

$unique_No=0;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'email_verified_at' => $faker->dateTimeBetween(),
        'password' => bcrypt($faker->password),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\aa_ProductWeb::class, function (Faker\Generator $faker) {


  $random_int = random_int(0,100);

    $input = array("Laptop", "Desktop", "Printer", "Webcam","Hardisk");

    $rand_item= array_rand($input, 1);

    $item_name = $input[$rand_item]." ".$random_int;

    // 'category_id' => $category_id
    //

    /*
    $category_id =0;

    switch ($rand_item)
    {
        case "Laptop":
            $category_id=0;
            break;
        case "Desktop":
            $category_id=1;
            break;
        case "Printer":
            $category_id=2;
            break;
        case "Webcam":
            $category_id=3;
            break;

        case "Hardisk":
            $category_id=4;
            break;
    }
*/


    return [
        'name' => $item_name,
        'slug' => str_slug($item_name, "-"),
        'details' => $faker->word,
        'price' => random_int(50000,100000),
        'description' => $faker->text,

    ];


});

