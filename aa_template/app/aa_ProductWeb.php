<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class aa_ProductWeb extends Model
{
    //

    public function formatPrice()
    {
        return money_format('$%i', $this->price / 100);
    }


    public function scopeGiveRandomLikes($query)
    {
        return $query->inRandomOrder()->take(4);
    }

}
